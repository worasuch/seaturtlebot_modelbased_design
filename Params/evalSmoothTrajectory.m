function out = evalSmoothTrajectory(params,t)
% Evaluates cubic spline trajectory given the following inputs
%   params  - Curve parameters (coefficients, time vector, etc.)
%   t       - Time to evaluate curve

    % Wrap the time value on every cycle of the trajectory
    tEff = mod(t,params.gaitPeriod);
    
    % Find the index of the curve segment based on time
    indices = find(tEff >= params.gaitTime(1:end-1));
    idx = indices(end);
    
    % Find the difference between current time and start of curve segment
    dt = tEff - params.gaitTime(idx);
    
    % Calculate smooth trajectory joint angles
    out = zeros(2,1); 
    out(1) = params.a0_foreAft(idx) + params.a1_foreAft(idx)*dt + ...
             params.a2_foreAft(idx)*dt^2 + params.a3_foreAft(idx)*dt^3;
    out(2) = params.a0_upDown(idx) + params.a1_upDown(idx)*dt + ...
             params.a2_upDown(idx)*dt^2 + params.a3_upDown(idx)*dt^3;

end