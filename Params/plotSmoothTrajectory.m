% Plots cubic spline trajectory, defined by the 'curveData' variable
%% Define vectors for plots
N = 500;
t = linspace(0,curveData.gaitPeriod,N);
foreAft_curve = zeros(size(t));
upDown_curve = zeros(size(t));

%% Loop over all points
for idx = 1:N 
    trajPts = evalSmoothTrajectory(curveData,t(idx));
    foreAft_curve(idx) = trajPts(1);
    upDown_curve(idx) = trajPts(2);   
end

%% Plot
figure
subplot(2,1,1)
plot(t,rad2deg(foreAft_curve),'b-', ...
     curveData.gaitTime,rad2deg([curveData.a0_foreAft;curveData.a0_foreAft(1)]),'ro');
xlabel('Time [s]');
ylabel('foreAft Angles [deg]');
subplot(2,1,2)
plot(t,rad2deg(upDown_curve),'b-', ...
     curveData.gaitTime,rad2deg([curveData.a0_upDown;curveData.a0_upDown(1)]),'ro');
xlabel('Time [s]');
ylabel('upDown Angles [deg]');