function curveData = createSmoothTrajectory(foreAft,upDown,period)
% Generates cubic spline trajectory parameters given waypoints
% for 2 joints (foreAft, upDown) and total gait period
% Source: https://see.stanford.edu/materials/aiircs223a/handout6_Trajectory.pdf

    %% Create necessary values for calculations
    numPoints = numel(upDown);
    curveData.gaitPeriod = period;
    curveData.gaitTime = linspace(0,period,numPoints);
    dt = period/(numPoints-1);
    
    %% Calculate derivatives
    % Assume zero derivatives at start and end
    upDown_der = [0; 0.5*( diff(upDown(1:end-1)) + diff(upDown(2:end)) )/dt; 0];
    foreAft_der = [0; 0.5*( diff(foreAft(1:end-1)) + diff(foreAft(2:end)) )/dt; 0];
    
    %% Do cubic spline fitting
    curveData.a0_upDown = upDown(1:end-1);
    curveData.a1_upDown = upDown_der(1:end-1);
    curveData.a2_upDown = 3*diff(upDown)/(dt^2) - 2*upDown_der(1:end-1)/dt - upDown_der(2:end)/dt;
    curveData.a3_upDown = -2*diff(upDown)/(dt^3) + (upDown_der(1:end-1)+upDown_der(2:end))/(dt^2);
    curveData.a0_foreAft = foreAft(1:end-1);
    curveData.a1_foreAft = foreAft_der(1:end-1);
    curveData.a2_foreAft = 3*diff(foreAft)/(dt^2) - 2*foreAft_der(1:end-1)/dt - foreAft_der(2:end)/dt;
    curveData.a3_foreAft = -2*diff(foreAft)/(dt^3) + (foreAft_der(1:end-1)+foreAft_der(2:end))/(dt^2);
