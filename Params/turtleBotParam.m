% Sea-TurtleBot Parameters

%% General parameters
density = 1000;
flipper_density = 2000;
world_damping = 0.25;
world_rot_damping = 0.25;

%% Contact/friction parameters
contact_stiffness = 2500;
contact_damping = 100;
mu_k = 0.6;
mu_s = 0.8;
mu_vth = 0.1;
height_plane = 0.025;
plane_x = 10;
plane_y = 3;
contact_point_radius = 1e-4;

%% Flipper parameters
flipper_x = 0.3;
flipper_y = 7;
flipper_z = 4;

%% Limb parameters
limb_radius = 0.5;
upper_limb_length = 9; 
lower_limb_length = 3;

%% Body parameters
body_x = 19;
body_y = 9;
body_z = 1;
body_offset_z = -0.5;
body_offset_x = -0.5;
init_height = 10;

%% Joint parameters
joint_damping = 1;
joint_stiffness = 1;
motion_time_constant = 0.01;

%% Joint controller parameters
upDown_servo_kp = 60;
upDown_servo_ki = 10;
upDown_servo_kd = 20;
foreAft_servo_kp = 60;
foreAft_servo_ki = 5;
foreAft_servo_kd = 10;
deriv_filter_coeff = 100;
max_torque = 20;

%% Electric motor parameters
motor_resistance = 1;
motor_constant = 0.02;
motor_inertia = 0;
motor_damping = 0;
motor_inductance = 1.2e-6;
gear_ratio = 50;

%% Turtle gait
gaitPeriod = 4;
time = linspace(0,gaitPeriod,3)';
foreAft = deg2rad([0, 0, 45, 45, 0]');
upDown = deg2rad([-45, 0, 0, -45, -45]');
curveData = createSmoothTrajectory(foreAft,upDown,gaitPeriod);