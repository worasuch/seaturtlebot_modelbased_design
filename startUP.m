%% Clear everything
clc
clear
close all

%% Add folders to the path
addpath(genpath('Params'),genpath('Libraries'))

%% Open the main model and load parameters
turtleBotParam
